# CMAD Main Repository
-----------

[TOC]

## Overview
This project is done as part of a cohort training to learn the Modern Application Development ecosystem. It attempts to emulate the basic stackoverflow functionality to demonstrate the various technologies learnt.

## Architecture
### Application Architecture
![Application Architecture](./docs/ApplicationArchitecture.jpg)

### Deployment Architecture
![Deployment Architecture](./docs/DeploymentArchitecture.jpg)

### CI / CD
![Continuous Integration](./docs/ContinuousIntegration.jpg)

## Components
### Web UI
* Repository: https://bitbucket.org/derrickpaul/cmad-ui
* Demo URL: http://cmad-ui.learnitall.club
### Questions Service
* Repository: https://bitbucket.org/derrickpaul/cmad-api-questions
* API Documentation: http://cmad-apis.learnitall.club/static/questions/swagger-ui/index.html
### Users Service
* Repository: https://bitbucket.org/derrickpaul/cmad-api-users
* API Documentation: http://cmad-apis.learnitall.club/static/users/swagger-ui/index.html
### Ops
* Jenkins: https://cmad-jenkins.learnitall.club
* SonarQube: https://cmad-sonar.learnitall.club
* Docker Hub: https://hub.docker.com/r/derrickpaul

## How to run locally
### Run from code
Details to run from code can be found in the individual repository README.md files.
### Run using Docker Compose
 * Note: An \*nix based system is prefered for running the following steps.
 * Install Docker CE (v1.13 or greater) : https://docs.docker.com/compose/install  
 * Docker Compose (v1.14 or greater) : https://github.com/docker/compose/releases  
 * Clone or download this GIT repository : https://bitbucket.org/derrickpaul/cmad-overview  
 * `cd cmad-ui/local-deploy`  
 * Review and edit ports in the docker-compose.yml file as per your system needs.  
 * Run `docker-compose up -d`. This brings up four containers - questions, users, ui and mongodb. If you use the given ports, you can access the services locally using the following URLs:
	* Questions API (docs) : http://localhost:8082/static/users/questions/swagger-ui/index.html
	* Users API (docs) : http://localhost:8084/static/users/questions/swagger-ui/index.html
	* Web UI : http://localhost:8080/

## Key learning
#### Application backend
[microservices](http://microservices.io/patterns/microservices.html), 
[vertx](http://vertx.io/),
[java-8](https://www.javatpoint.com/java-8-features), 
[js-in-java](http://www.oracle.com/technetwork/articles/java/jf14-nashorn-2126515.html), 
[polyglot](https://dzone.com/articles/polyglot-programming-good),
[NoSQL](https://en.wikipedia.org/wiki/NoSQL), 
[MongoDB](https://www.mongodb.com/), 
[hazelcast](https://hazelcast.org/), 
[contract-first-rest](https://www.slideshare.net/JohnZaccone/api-documentation-languages-and-contract-first-design), 
[swagger](https://swagger.io/)

#### Application frontend
[js-ES6](http://www.tutorialspoint.com/es6/), 
[React](https://facebook.github.io/react/), 
[Redux](http://redux.js.org/),
[learn-react-redux](https://github.com/coryhouse/pluralsight-redux-starter),
[web-sockets](https://en.wikipedia.org/wiki/WebSocket), 
[nodejs](https://nodejs.org), 
[webpack](https://webpack.js.org/), 
[serve](https://www.npmjs.com/package/serve)

#### Ops
[continuous-integration](https://en.wikipedia.org/wiki/Continuous_integration), 
[continuous-delivery](https://continuousdelivery.com/)
[cdelivery-vs-cdeployment](https://puppet.com/blog/continuous-delivery-vs-continuous-deployment-what-s-diff),
[ci-cd](https://www.digitalocean.com/community/tutorials/an-introduction-to-continuous-integration-delivery-and-deployment),
[Docker](https://www.docker.com/), 
[docker-compose](https://docs.docker.com/compose/), 
[docker-hub](https://hub.docker.com/), 
[Jenkins](https://jenkins.io/), 
[SonarQube](https://www.sonarqube.org/), 
[JenkinsSparkNotifier](https://wiki.jenkins.io/display/JENKINS/Spark+Notifier+Plugin)

#### Cloud
[cloud-native](https://pivotal.io/cloud-native),
[GCP](https://cloud.google.com/),
[aws](https://aws.amazon.com/)
[GCE](https://cloud.google.com/compute/),
[GKE](https://cloud.google.com/container-engine/),
[gcloud-load-balancer](https://cloud.google.com/load-balancing/),
[kubernetes](https://kubernetes.io/docs/concepts/)

## Next steps / areas for improvement
 * Consider NFRs (performance, security, etc.) 
 * Multi-node MongoDB.
 * Vertx Event bus cluster with websockets.
 * Enhance UI responsiveness for mobile.
 * Tracking code coverage.
 * UI automated testing as part of build.